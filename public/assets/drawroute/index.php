<?php
//require ("DBController.php");

define("API_KEY", "AIzaSyD-vRssronJdmyuzEpFUcz2ZDNxQrkRiQ0");
 
 
$countryResult[0]['lat'] = "30.2422085";
$countryResult[0]['lng'] = "75.8077663";
$countryResult[1]['lat'] = "30.2441545";  
$countryResult[1]['lng'] = "75.836822"; 
$countryResult[2]['lat'] = "30.233885"; 
$countryResult[2]['lng'] = "75.8361923";
$countryResult[3]['lat'] = "30.2422085";
$countryResult[3]['lng'] = "75.8077663";

?>
<html>
<head>
<title>Show Path on Google Map using Javascript API</title>
<style>
body {
	font-family: Arial;
}

#map-layer {
	margin: 20px 0px;
	max-width: 600px;
	min-height: 400;
}
</style>
</head>
<body  >
	<h1>Show Path on Google Map using Javascript API</h1>
	<div id="map-layer"></div>
	<script>
      	var map;
		var pathCoordinates = Array();
      	function initMap() {
        	  	var countryLength
            	var mapLayer = document.getElementById("map-layer"); 
            	var centerCoordinates = new google.maps.LatLng(30.2422085, 75.80776635);
        		var defaultOptions = { center: centerCoordinates, zoom: 15,mapTypeId: 'satellite' }
        		map = new google.maps.Map(mapLayer, defaultOptions);
        		geocoder = new google.maps.Geocoder();
        	    <?php
            if (! empty($countryResult)) {
            ?>
            countryLength = <?php echo count($countryResult); ?>
            <?php
                foreach ($countryResult as $value ) 
                {
            ?>  
             	
                            var latitude = <?php echo $value['lat'] ?>;
                            var longitude = <?php echo $value['lng'] ?>;
                            pathCoordinates.push({lat: latitude, lng: longitude});
                            
                         /*   new google.maps.Marker({
                                position: new google.maps.LatLng(latitude, longitude),
                                map: map,
                                title: 'df',

                            });
                            */
                            if(countryLength == pathCoordinates.length) {
                                drawPath();
                            }
                            
                        
        	    <?php
                }
            }
            ?>	
      	}
        	function drawPath() {
            	new google.maps.Polyline({
                  path: pathCoordinates,
                  geodesic: true,
                  strokeColor: '#FF0000',
                  strokeOpacity: 1,
                  strokeWeight: 2,
                  map: map
            });
        }
	</script>
	<script async defer
		src="https://maps.googleapis.com/maps/api/js?key=<?php echo API_KEY; ?>&callback=initMap">
    </script>
</body>
</html>