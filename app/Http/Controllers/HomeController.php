<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // pa('hi');die();
         $param =$data = array();
       
        // pa($param);
        $forest_about = call_api('forest_about', $data);
        $forest_annual_operation = call_api('forest_annual_operation', $data);
        $forest_latest_news = call_api('forest_latest_news', $data);
        $forest_notification = call_api('forest_notification', $data);
        $forest_event = call_api('forest_event', $data);
        $forest_important_contact = call_api('forest_important_contact', $data);
        $forest_support = call_api('forest_support',$data);
        $forest_event_action = call_api('forest_event_action',$data);
        $forest_statement = call_api('forest_statement',$data);
        $activity_under_pscampa = call_api('activity_under_pscampa',$data);
        $activity_pscampa = call_api('activity_pscampa',$data);
        $activity_sub_pscampa = call_api('activity_sub_pscampa',$data);

        // pa($result); die();
        $data = array();

        $data['forest_about'] = $forest_about->Forest_about_list;

        $data['forest_annual_operation'] = $forest_annual_operation->forest_annual_operstion_list;

        $data['forest_latest_news'] = $forest_latest_news->Latest_news_etail;

        $data['forest_notification'] = $forest_notification->forest_notification_list;

        $data['forest_event'] = $forest_event->forest_event_list;

        $data['forest_important_contact'] = $forest_important_contact->forest_important_contact_list;

        $data['forest_support'] = $forest_support->Support_list;

        $data['forest_event_action'] = $forest_event_action->Forest_event_and_action_list;

        $data['forest_statement'] = $forest_statement->Statement_detail;

        $data['activity_under_pscampa'] = $activity_under_pscampa->pscampa_list;

        $data['activity_pscampa'] = $activity_pscampa->ps_campa_list;

        $data['activity_sub_pscampa'] = $activity_sub_pscampa->ps_list;

        // pa($data);  die();
        return view('/index',$data);
    }
}
