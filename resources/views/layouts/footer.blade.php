  
        <footer>
            <div class="container">
                <div class="foot-links text-center">
                    <ul class="list-inline mb-0">
                        <li class="list-inline-item">
                            <a href="index.html"> Home </a>
                        </li>
                        <li class="list-inline-item"> | </li>
                        <li class="list-inline-item">
                            <a href=""> Tenders </a>
                        </li>
                        <li class="list-inline-item"> | </li>
                        <li class="list-inline-item">
                            <a href=""> Accessibility Statement </a>
                        </li>
                        <li class="list-inline-item"> | </li>
                        <li class="list-inline-item">
                            <a href=""> Website Policies </a>
                        </li>
                        <li class="list-inline-item"> | </li>
                        <li class="list-inline-item">
                            <a href=""> Terms & Conditions </a>
                        </li>
                        <li class="list-inline-item"> | </li>
                        <li class="list-inline-item">
                            <a href=""> Disclaimer </a>
                        </li>
                        <li class="list-inline-item"> | </li>
                        <li class="list-inline-item">
                            <a href=""> Feedback </a>
                        </li>
                        <li class="list-inline-item"> | </li>
                        <li class="list-inline-item">
                            <a href=""> Help </a>
                        </li>
                        <li class="list-inline-item"> | </li>
                        <li class="list-inline-item">
                            <a href=""> Sitemap </a>
                        </li>
                        <li class="list-inline-item"> | </li>
                        <li class="list-inline-item">
                            <a href=""> Contact Us </a>
                        </li>
                    </ul>
                </div>
            </div>
            
            <div class="hr"></div>
            
            <div class="bottom-bar">
                <div class="container">
                    <p class="mb-0 text-center"> &copy; 2019 | All Rights Reserved </p>
                    <div class="text-right">
                        <ul class="list-inline list-unstyled mb-0">
                            <li class="list-inline-item">
                                <a href="http://jigsaw.w3.org/css-validator/check/referer">
                                    <img style="border:0;width:60px;height:21px" src="http://jigsaw.w3.org/css-validator/images/vcss" alt="Valid CSS!" />
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>
        <!-- footer -->

        <!-- JavaScript -->
        <script src="../assets/js/bootstrap.min.js"></script>
        <script src="../assets/js/bootstrap.min.js.map"></script>
        <script src="../assets/js/jquery-3.4.1.min.js"></script>
        <script src="../assets/js/jquery.counterup.min.js"></script>
        <script src="../assets/js/jquery.fancybox.min.js"></script>
        <script src="../assets/js/jquery.waypoints.min.js"></script>
        <script src="../assets/js/owl.carousel.min.js"></script>
        <script src="../assets/js/popper.min.js"></script>

        <script>
            var $affectedElements = $("li, p, td, th"); // Can be extended, ex. $("div, p, span.someClass")

            // Storing the original size in a data attribute so size can be reset
            $affectedElements.each( function(){
                var $this = $(this);
                $this.data("orig-size", $this.css("font-size") );
            });

            $("#btn-increase").click(function(){
                changeFontSize(1);
            })
            $("#btn-decrease").click(function(){
                changeFontSize(-1);
            })

            $("#btn-orig").click(function(){
                $affectedElements.each( function(){
                    var $this = $(this);
                    $this.css( "font-size" , $this.data("orig-size") );
                });
            })

            function changeFontSize(direction){
                $affectedElements.each( function(){
                    var $this = $(this);
                    $this.css( "font-size" , parseInt($this.css("font-size"))+direction );
                });
            }
        </script>
        <script>
            function dark_light() {
                var element = document.body;
                element.classList.toggle("dark-mode");
            }
        </script>
        <script>
            $('.counter').counterUp({
                delay: 10,
                time: 1000,
                offset: 70
            });
        </script>
        
     
        <script>
            $('.events').owlCarousel({
                loop: true,
                margin: 10,
                lazyLoad: true,
                onTranslated: animateSlide,
                onTranslate: removeAnimation,
                autoplay: true,
                autoplayTimeout: 3000,
                autoplayHoverPause: false,
                nav: false,
                dots: true,
                smartSpeed: 900,
                responsiveClass: true,
                responsive:{
                    0:{
                        items:1
                    },
                    600:{
                        items:2
                    },
                    1000:{
                        items:3
                    }
                }
            });


            // Other Slides
            function removeAnimation() {
               var item = $(".owl-item");
              item.removeClass(item.children().data('animate'));

            }

            function animateSlide() {

              var item = $(".owl-item.active");
              item.addClass(item.children().data('animate'));


            }
        </script>

        <script src="../assets/js/jquery.marquee.js"></script>
        <script>
            $('#news').marquee();
        </script>
         @stack('page-script')
    </body>
</html>