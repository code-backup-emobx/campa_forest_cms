<!DOCTYPE html>
<html lang="en">
    <head>

        <!-- CSS -->
        
        <!-- meta tags -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <!-- CSS -->
        <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="../assets/css/style.css">
        <link rel="stylesheet" href="../assets/css/all.min.css">
        <link rel="stylesheet" href="../assets/css/owl.carousel.min.css">
        <link rel="stylesheet" href="../assets/css/owl.theme.default.min.css">
        <link rel="stylesheet" href="../assets/css/animate.css">

        <title> CAMPA Punjab </title>
    </head>
    <body>
        <header>
            <div class="top-bar">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3 col-3">
                            <div>
                                <img src="../assets/img/digital.jpg" alt="" class="img-fluid">
                            </div>
                        </div>
                        <div class="col-md-9 col-9">
                            <div class="text-right">
                                <ul class="list-inline mb-0">
                                    <li class="list-inline-item">
                                        <button class="btn btn-font" id="btn-decrease" title="Font Decrease"> A- </button>
                                        <button class="btn btn-font" id="btn-orig" title="Original Font"> A </button>
                                        <button class="btn btn-font" id="btn-increase" title="Font Increase"> A+ </button>
                                    </li>
                                    <li class="list-inline-item"> | </li>
                                    <li class="list-inline-item">
                                        <button class="btn btn-dl" onclick="dark_light()" title="Dark/Light Mode"> A </button>
                                    </li>
                                    <li class="list-inline-item"> | </li>
                                    <li class="list-inline-item">
                                        <!--<a href="" class="btn btn-icon" title="Facebook"> <i class="fab fa-facebook-f"></i> </a>
                                        <a href="" class="btn btn-icon" title="Twitter"> <i class="fab fa-twitter"></i> </a>-->
                                        <a href="" class="btn btn-icon" title="RSS"> <i class="fas fa-rss"></i> </a>
                                        <a href="" class="btn btn-icon" title="Sitemap"> <i class="fas fa-sitemap"></i> </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- topbar -->
            
            <div class="navbar-div">
                <div class="container">
                    <nav class="navbar navbar-expand-lg navbar navbar-dark">
                        <a class="navbar-brand" href="/">
                            <img src="../assets/img/logo_trans.png" alt="" class="img-fluid">
                        </a>
                        
                        <div class="row navbar-nav ml-auto">
                            <div class="col-md-5 col-9 offset-3 nav-item web_name">
                                Punjab State Compensatory Afforestation Management and Planning Authority
                            </div>
                            <div class="col-md-4 col-12 nav-item search_input">
                                <div class="input-group">
                                    <input class="form-control border-right-0" type="text" value="Type to search....." placeholder="Type to search.....">
                                    <span class="input-group-append bg-white">
                                        <button class="btn border border-left-0" type="button"><i class="fas fa-search"></i></button>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-2 col-6 nav-item user_log">
                                <i class="fas fa-user-lock"></i>
                               <a href="http://test.esolx.net/campa_forest/campalogin/login.php"> <p class="mb-0"> Login/Register </p></a>
                            </div>
                            <div class="col-md-1 col-6 nav-item dropdown has-mega-menu" style="position:static;">
                                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-bars"></i>
                                </a>
                                
                                <div class="dropdown-menu" style="width:100%">
                                    <div class="px-0">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <ul class="list-unstyled">
                                                    <li>
                                                        <a class="dropdown-item" href="#"> Introduction </a>
                                                        <ul class="">
                                                            <li>
                                                                <a href=""> Guidelines of Punjab State CAMPA </a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <a class="dropdown-item" href=""> Execution of Works </a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="col-md-3">
                                                <ul class="list-unstyled">
                                                    <li>
                                                        <a class="dropdown-item" href=""> Annual Plan of Operations (APO) </a>
                                                        <ul class="">
                                                            <li>
                                                                <a href=""> Preparation of Annual Plan of Operations </a>
                                                            </li>
                                                            <li>
                                                                <a href=""> Approval of APO </a>
                                                                <ul class="">
                                                                    <li>
                                                                        <a href=""> Release of Funds </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href=""> Cash Book </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href=""> Maintenance of Records </a>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                            <li>
                                                                <a href=""> Monitoring & Evaluation </a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="col-md-3">
                                                <ul class="list-unstyled">
                                                    <li>
                                                        <a class="dropdown-item" href=""> Sanction of Estimates </a>
                                                        <ul class="">
                                                            <li>
                                                                <a href=""> Preparation of Estimates </a>
                                                            </li>
                                                            <li>
                                                                <a href=""> Sanction of Estimates </a>
                                                            </li>
                                                            <li>
                                                                <a href=""> Filing of Completion Reports (CRs) </a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="col-md-3">
                                                <ul class="list-unstyled">
                                                    <li>
                                                        <a class="dropdown-item" href="#"> Preparation of Bills and Payment </a>
                                                        <ul class="">
                                                            <li>
                                                                <a href=""> Delegation of powers on passing for payment of single bill </a>
                                                            </li>
                                                            <li>
                                                                <a href=""> Payment of bills </a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
            <!-- navbar -->
        </header>
        