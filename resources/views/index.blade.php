@extends('layouts.master')
@section('content')
        
        <div class="main-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <div class="block">
                            <p class="mb-0 ribbon"> About </p>
                            <ul class="list-unstyled">
                               
                                @foreach($forest_about as $res)

                                    <li>
                                        <a href="">  {{ $res->about_text}} </a>
                                    </li>
                                @endforeach
                               
                            </ul>
                        </div><!-- block1 -->
                        
                        <div class="block">
                            <p class="mb-0 ribbon"> Annual Plan of Operations (APO) </p>
                            <ul class="list-unstyled">
                                @foreach($forest_annual_operation as $listing)
                                <li>
                                    <a href=""> {{ $listing->annual_text}} </a>
                                </li>
                                @endforeach
                            </ul>
                        </div><!-- block2 -->
                        
                        <div class="block">
                            <p class="mb-0 ribbon"> Activities under Punjab State Compensatory Afforestation Management and Planning Authority (PSCAMPA) [during 2019-20] </p>
                            <ul class="list-unstyled">
                                 <li>
                                    <a href=""> Plantation </a>
                                    <ul class="list-unstyled">
                                        @foreach( $activity_under_pscampa as $listing)
                                            <li>
                                                <a href=""> {{ $listing->activity_name }} </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </li>
                                <li>
                                    <a href=""> Other Activities </a>
                                    <ul class="list-unstyled">
                                        @foreach($activity_pscampa as $listing)
                                        <li>
                                            <a href=""> {{ $listing->activity_name}} </a>
                                        </li>
                                        @endforeach
                                        <li>
                                            <a href=""> Extension Activities </a>
                                            <ul class="list-unstyled">
                                                @foreach($activity_sub_pscampa as $listing)
                                                    <li>
                                                        <a href=""> {{ $listing->sub_activity_name }} </a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div><!-- block3 -->
                        
                        <div class="block">
                            <p class="mb-0 ribbon"> Monitoring and Evaluation </p>
                            <!--<p class="text-right view_all"> <a href="">View All</a> </p>
                            <ul class="list-unstyled">
                                <li>
                                    <a href="">Press Note - MoEF (08/10/2012)</a>
                                </li>
                            </ul>-->
                        </div><!-- block4 -->
                        
                    </div>
                    <!-- leftbar -->
                    
                    <div class="col-md-6">
                        <div class="row info-content">
                            <div class="col-md-12">
                                <div class="block cm">
                                    <p class="mt-0 h4"> Statement </p>
                                    <div class="statement_img float-right">
                                        <img src="{{ env('APP_URL').($forest_statement[0]->media) }}" class="img-fluid float-right" alt="">
                                        <p class="mb-0"> Chief Minister of Punjab - Captain Amrinder Singh </p>
                                    </div>
                                    <p class="mb-0">
                                        {{ $forest_statement[0]->description }}
                                    </p>
                                </div>
                                
                                <div class="block">
                                    <p class="mt-0 h4"> Events & Actions </p>
                                    <div class="events owl-carousel owl-theme">
                                        @foreach($forest_event_action as $listing)
                                        <div data-animate="bounceIn animated"> <img src="{{ env('APP_URL').($listing->picture) }}" alt="" class="img-fluid"> </div>
                                        @endforeach
                                        
                                    </div>
                                </div>
                                
                                <div class="row graphs">
                                    <div class="col-md-4">
                                        <div class="block">
                                            <p class="mb-0 ribbon"> Financial detail of APO during last five years </p>
                                            <img src="../assets/img/1.png" alt="" class="img-fluid mx-auto d-block">
                                        </div>
                                    </div>
                                        
                                    <div class="col-md-4">
                                        <div class="block">
                                            <p class="mb-0 ribbon"> Plantation during five years </p>
                                            <img src="../assets/img/2.png" alt="" class="img-fluid mx-auto d-block">
                                        </div>
                                    </div>
                                        
                                    <div class="col-md-4">
                                        <div class="block">
                                            <p class="mb-0 ribbon"> State of forest in Punjab </p>
                                            <img src="../assets/img/3.png" alt="" class="img-fluid mx-auto d-block">
                                        </div>
                                    </div>
                                        
                                    <div class="col-md-4">
                                        <div class="block">
                                            <p class="mb-0 ribbon"> Division-wise plantation progress </p>
                                            <img src="../assets/img/4.png" alt="" class="img-fluid mx-auto d-block">
                                        </div>
                                    </div>
                                            
                                    <div class="col-md-4">
                                        <div class="block">
                                            <p class="mb-0 ribbon"> Division-wise financial </p>
                                            <img src="../assets/img/4.png" alt="" class="img-fluid mx-auto d-block">
                                        </div>
                                    </div>
                                        
                                    <div class="col-md-4">
                                        <div class="block">
                                            <p class="mb-0 ribbon"> Total No. of trees </p>
                                            <img src="../assets/img/5.png" alt="" class="img-fluid mx-auto d-block">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- center div -->
                    
                    <div class="col-md-3">
                        <div class="news-block">
                            <ul id="news">
                                @foreach( $forest_latest_news as $latest_news)
                                <li> 
                                    <a href=""> {{ $latest_news->news_heading }} </a>
                                    <p class="text-muted mb-0">
                                        <small> {{ $latest_news->news_date }} </small>
                                    </p>
                                    <p> {{ $latest_news->news_description }} </p>
                                </li>
                              @endforeach
                            </ul>
                        </div>
                        <!-- news-block -->
                        
                        <div class="block">
                            <p class="mb-0 ribbon"> Notifications / Circulars / Orders </p>
                            <p class="text-right view_all"> <a href="">View All</a> </p>
                            <ul class="list-unstyled">

                                @foreach($forest_notification as $notification_listing)
                                <li>
                                    <a href=""> {{ $notification_listing->notification_text }} </a>
                                </li>
                               @endforeach
                            </ul>
                        </div><!-- block1 -->
                        
                        <div class="block">
                            <p class="mb-0 ribbon"> Events </p>
                            <p class="text-right view_all"> <a href="">View All</a> </p>
                            <ul class="list-unstyled">
                                @foreach($forest_event as $listing)
                                    <li>
                                        <a href="">{{ $listing->event_text }}</a>
                                    </li>
                                @endforeach    
                               
                            </ul>
                        </div><!-- block2 -->
                        
                        <div class="block">
                            <p class="mb-0 ribbon"> Important Contacts </p>
                            <ul class="list-unstyled">
                                <li>
                                    List of: 
                                    @foreach($forest_important_contact as $listing)
                                    <a href="">{{ $listing->contact_text }}</a>,
                                    @endforeach
                                </li>
                            </ul>
                        </div><!-- block3 -->
                        
                        <div class="block">
                            <p class="mb-0 ribbon"> Support </p>
                            <ul class="list-unstyled">
                                @foreach($forest_support as $listing)
                                    <li>
                                        <a href="">{{ $listing->support_text }}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div><!-- block4 -->
                    </div>
                    <!-- rightbar -->
                </div>
            </div>
        </div>
        <!-- content -->
        @endsection
      